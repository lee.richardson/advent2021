﻿#load "Utils.fsx"

open Utils
open FParsec.Primitives
open FParsec.CharParsers

let numberOfIncreases =
    Seq.windowed 2
    >> Seq.sumBy (fun [|a; b|] -> if b > a then 1 else 0)

let part1 = numberOfIncreases

let part2 =
    Seq.windowed 3
    >> Seq.map Seq.sum
    >> numberOfIncreases

Utils.go 1 (many (pint32 .>> opt newline)) part1 part2
