#load "Utils.fsx"

open Utils
open FParsec.Primitives
open FParsec.CharParsers

let part1 =
    Seq.fold
        (fun (y, z) current ->
            match current with
            | ("forward", a) -> (y + a, z)
            | ("up", a)      -> (y, z - a)
            | ("down", a)    -> (y, z + a)
            | _              -> (y, z))
        (0, 0)
    >> (fun (y, z) -> (y * z))

let part2 =
    Seq.fold
        (fun (y, z, aim) current ->
            match current with
            | ("forward", a) -> (y + a, z + (a * aim), aim)
            | ("up", a)      -> (y, z, aim - a)
            | ("down", a)    -> (y, z, aim + a)
            | _              -> (y, z, aim))
        (0, 0, 0)
    >> (fun (y, z, _) -> (y * z))

let parser = manyCharsTill anyChar spaces1 .>>. pint32

Utils.go 2 (many (parser .>> opt newline)) part1 part2
