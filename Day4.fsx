#load "Utils.fsx"

open System
open Utils
open FParsec.Primitives
open FParsec.CharParsers
open Microsoft.FSharp.Collections

let hasBoardWon (boardMarks: bool array array) =
    let rowWins = Array.exists (Array.forall ((=) true))
    rowWins boardMarks || rowWins (Array.transpose boardMarks)

let part1 (input: int32 list * int32 list list list) =
    let drawnNumbers = fst input
    let boards       = snd input |> Array.ofList
    let boardMarks   = Array.init (Array.length boards) (fun _ -> (Array.init 5 (fun _ -> Array.create 5 false)))
    let boardAndMarks = Array.zip boards boardMarks

    let markNumber (number: int32) (board) (boardMarks: bool array array) =
        let y = List.tryFindIndex (fun b -> List.contains number b) board
        if y.IsSome then
            let x = List.findIndex ((=) number) board.[y.Value]
            boardMarks.[y.Value].[x] <- true
    
    let lastDrawn =
        List.skipWhile
            (fun draw ->
                Array.iter (fun (b, bm) -> markNumber draw b bm) boardAndMarks
                not <| Array.exists (snd >> hasBoardWon) boardAndMarks)
            drawnNumbers
        |> List.head
    
    let winner = Array.find (snd >> hasBoardWon) boardAndMarks
    let sumUnmarked =
        Seq.zip (fst winner) (snd winner)
        |> Seq.map (fun x -> Seq.zip (fst x) (snd x))
        |> Seq.collect id
        |> Seq.sumBy (fun (b, m) -> if m then 0 else b)

    lastDrawn, sumUnmarked, sumUnmarked * lastDrawn

let part2 (input: int32 list * int32 list list list) =
    ()

let line1Parser = sepBy1 pint32 (pchar ',')
let boardRowParser = skipMany (pchar ' ') >>. sepEndBy1 pint32 (skipMany1 (pchar ' '))
let boardParser = sepEndBy1 boardRowParser newline
let inputParser = (tuple2 (line1Parser .>> many1 newline) (sepEndBy1 boardParser newline))

Utils.go 4 inputParser part1 part2


(* let testData = @"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"

Utils.goWithData testData inputParser part1 part2 *)

(* let testLine = " 8  2 23  4 24"

let boardRow = many (pchar ' ') >>. sepEndBy1 pint32 (skipMany (pchar ' '))

Utils.goWithData testLine boardRow log log

let testBoard = @"22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19"

let boardParser = sepEndBy1 boardRow newline

Utils.goWithData testBoard boardParser log log
 *)