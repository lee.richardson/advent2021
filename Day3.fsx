#load "Utils.fsx"

open System
open Utils
open FParsec.Primitives
open FParsec.CharParsers
open Microsoft.FSharp.Collections

let toDec (input: char seq) =
    input
    |> Seq.toArray
    |> String
    |> (fun b -> Convert.ToUInt32(b, 2))

let gamma (input: #seq<#seq<char>>) = 
    input
    |> Seq.transpose
    |> Seq.map (Seq.groupBy id)
    |> Seq.map (fun groups ->
        if 
            Seq.length groups > 1
            && groups
            |> Seq.map (snd >> Seq.length)
            |> Seq.distinct |> Seq.length = 1
        then '1'
        else (groups |> (Seq.maxBy (snd >> Seq.length))) |> fst)
    |> Seq.toList

let epsilon gamma = gamma |> Seq.map (function | '1' -> '0' | '0' -> '1') |> Seq.toList

let part1 (input: char list list) =
    let gamma      = gamma input
    let gammaNum   = toDec gamma
    let epsilonNum = toDec (epsilon gamma)
    gammaNum, epsilonNum, gammaNum * epsilonNum

let part2 (input: char list list) =
    let rec filterInputs bitFilter bitIdx input: char list =
        let input = input |> Seq.map Seq.toList |> Seq.toList
        if List.length input = 1
        then List.head input
        else

        if bitIdx >= 12 then failwith "Didn't find one."

        let comparisonBits = bitFilter input
        
        input
        |> List.filter (fun bits -> (List.item bitIdx bits) = (List.item bitIdx comparisonBits))
        //|> Seq.map Seq.ofList
        |> Seq.cast
        |> filterInputs bitFilter (bitIdx + 1)
        


    input
    |> (fun input ->
        (filterInputs gamma 0 input |> toDec,
        filterInputs (gamma >> epsilon) 0 input |> toDec)
    )
    ||> (fun o2 co2 -> (o2, co2, o2 * co2))

Utils.go 3 (many (many1 digit .>> opt newline)) part1 part2

(*
let testData = @"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"

Utils.goWithData testData (many (parser .>> opt newline)) part1 part2
*)