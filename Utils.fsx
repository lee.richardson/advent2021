﻿#r "nuget: FParsec"

module Utils =
    open FParsec.CharParsers

    [<AutoOpen>]
    module Basic =
        let inline tee f x = f x; x
        let inline flip f x y = f y x
        let (><) = flip

    [<AutoOpen>]
    module OptionOperators =
        let (>>=) x f = Option.bind f x
        let (>=>) f g a = f a >>= g
        let (<!>) = Option.map
        let (|>>) x f = Option.map f x

    let log it = printfn "%A" it
    let logSeq it = Seq.iter log it
    let logN n it = Seq.take n it |> logSeq
    let log10 it = logN 10 it

    let loadAdventInput day =
        System.IO.File.ReadAllText (sprintf "%i.txt" day)
   
    let inline goWithData data parser part1 part2 =
        run parser data
        |> function
            | Success (result, _, _) ->
                let sw = System.Diagnostics.Stopwatch ()
                
                printfn "Part 1"
                sw.Start ()
                printfn "%A" (part1 result)
                printfn "%A\n" sw.Elapsed
                
                printfn "Part 2"
                sw.Restart ()
                printfn "%A" (part2 result)
                printfn "%A" sw.Elapsed
                sw.Stop ()
            | Failure (errStr, _, _) -> eprintfn "%s" errStr

    let inline go day =
        goWithData (loadAdventInput day)